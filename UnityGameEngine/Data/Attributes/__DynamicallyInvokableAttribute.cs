﻿using System;

namespace System.ComponentModel.DataAnnotations {
    [AttributeUsage(AttributeTargets.All, Inherited = false)]
    internal sealed class __DynamicallyInvokableAttribute : Attribute {
        //[TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        public __DynamicallyInvokableAttribute() {
        }
    } 
}