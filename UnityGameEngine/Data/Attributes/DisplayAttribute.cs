﻿using System;
using System.Globalization;

namespace System.ComponentModel.DataAnnotations{
﻿   // Assembly: System.ComponentModel.DataAnnotations, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // Runtime: v4.0.30319
    // Architecture: MSIL
    /// <summary>
    /// DisplayAttribute is not included in the .NET 3.5 Framework, so in order to use it, I have to
    /// include the source code in the project.
    /// </summary>
        [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false), __DynamicallyInvokable]
        public sealed class DisplayAttribute : Attribute {
            private Type _resourceType;
            private string _shortName = "ShortName";
            private string _name = "Name";
            private string _description = "Description";
            private string _prompt = "Prompt";
            private string _groupName = "GroupName";
            private bool? _autoGenerateField;
            private bool? _autoGenerateFilter;
            private int? _order;
            [__DynamicallyInvokable]
            public string ShortName {
                [__DynamicallyInvokable]
                get {
                    return this._shortName;
                }
                [__DynamicallyInvokable]
                set {
                        this._shortName = value;
                }
            }
            [__DynamicallyInvokable]
            public string Name {
                [__DynamicallyInvokable]
                get {
                    return this._name;
                }
                [__DynamicallyInvokable]
                set {
                        this._name = value;
                }
            }
            [__DynamicallyInvokable]
            public string Description {
                [__DynamicallyInvokable]
                get {
                    return this._description;
                }
                [__DynamicallyInvokable]
                set {
                        this._description = value;
                }
            }
            [__DynamicallyInvokable]
            public string Prompt {
                [__DynamicallyInvokable]
                get {
                    return this._prompt;
                }
                [__DynamicallyInvokable]
                set {
                        this._prompt = value;
                }
            }
            [__DynamicallyInvokable]
            public string GroupName {
                [__DynamicallyInvokable]
                get {
                    return this._groupName;
                }
                [__DynamicallyInvokable]
                set {
                        this._groupName = value;
                }
            }
            [__DynamicallyInvokable]
            public Type ResourceType {
                [__DynamicallyInvokable]//, TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
                get {
                    return this._resourceType;
                }
                [__DynamicallyInvokable]
                set {
                    if (this._resourceType != value) {
                        this._resourceType = value;
                    }
                }
            }
            [__DynamicallyInvokable]
            public DisplayAttribute() {
            }
            [__DynamicallyInvokable]
            public string GetShortName() {
                return this._shortName ?? this.GetName();
            }
            [__DynamicallyInvokable]
            public string GetName() {
                return this._name;
            }
            [__DynamicallyInvokable]
            public string GetDescription() {
                return this._description;
            }
            [__DynamicallyInvokable]
            public string GetPrompt() {
                return this._prompt;
            }
            [__DynamicallyInvokable]
            public string GetGroupName() {
                return this._groupName;
            }
            [__DynamicallyInvokable]
            public bool? GetAutoGenerateField() {
                return this._autoGenerateField;
            }
            [__DynamicallyInvokable]
            public bool? GetAutoGenerateFilter() {
                return this._autoGenerateFilter;
            }
            [__DynamicallyInvokable]
            public int? GetOrder() {
                return this._order;
            }
        }
    } 