﻿using UnityGameEngine.Data.Model.Enum;

namespace UnityGameEngine.Data.Model.Item{
    public class BaseItemType : EnumEx<int>{
        protected BaseItemType() : base() {}

        protected BaseItemType(int value) : base(value){
        }

        public static implicit operator BaseItemType(int i){
            return new BaseItemType(i);
        }
    }
}