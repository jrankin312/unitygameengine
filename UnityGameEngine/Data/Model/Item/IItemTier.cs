﻿using System.Security.Cryptography.X509Certificates;

namespace UnityGameEngine.Data.Model.Item{
    public interface IItemTier {
        string Name(BaseItemType itemType);
    }
}