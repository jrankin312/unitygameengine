﻿using System.Collections.Generic;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Data.Model.Item {
    public interface IItemDropTier {
        //The monsterlevel this tier will start dropping at
        int RequiredLevel { get; set; }

        float DropWeight { get; set; }

        IList<BaseItemCategory> ItemCategories { get; set; }
        IList<BaseItemType> ItemTypes { get; set; }

        IList<BaseStat> StatRanges { get; set; }
    }
}