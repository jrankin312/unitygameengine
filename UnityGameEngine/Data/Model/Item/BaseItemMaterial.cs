﻿using UnityGameEngine.Data.Model.Enum;

namespace UnityGameEngine.Data.Model.Item{
    public class BaseItemMaterial : EnumEx<int>{
        protected BaseItemMaterial() : base() {}

        protected BaseItemMaterial(int value) : base(value) {
        }

        public static implicit operator BaseItemMaterial(int i) {
            return new BaseItemMaterial(i);
        }
    }
}