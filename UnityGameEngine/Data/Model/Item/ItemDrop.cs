﻿using System.Collections.Generic;

namespace UnityGameEngine.Data.Model.Item{
    public class ItemDrop : IItemContainer {

        public List<BaseItem> Items { get; set; }

        public ItemDrop(){
            Items = new List<BaseItem>();
        }

        public ItemDrop AddItem(BaseItem item){
            Items.Add(item);
            return this;
        }
    }
}