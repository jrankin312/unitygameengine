﻿using UnityGameEngine.Data.Model.Enum;

namespace UnityGameEngine.Data.Model.Item{
    public class BaseItemCategory : EnumEx<int>{
        protected BaseItemCategory() : base() {}

        protected BaseItemCategory(int value)
            : base(value) {
        }

        public static implicit operator BaseItemCategory(int i) {
            return new BaseItemCategory(i);
        }
    }
}