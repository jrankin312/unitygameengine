﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityGameEngine.Data.Model.Enum;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Data.Model.Item {
    public class BaseItem : BaseData, IHasTooltip<BaseItem> {
        public IItemContainer Owner { get; set; }

        public BaseItemCategory Category { get; set; }
        public BaseItemType Type { get; set; }

        public BaseItemMaterial Material { get; set; }
        public int Quality { get; set; }

        public int RequiredLevel {
            get{
                return Quality*5;
            }
        }

        public ContextMenu<BaseItem> Menu { get; set; }

        public bool IsEquippable { get; set; }
        public IList<StatBonus> Stats { get; set; }

        public bool IsConsumable { get; set; }
        public IList<BaseStatModifier> StatModifiers { get; set; } 

        public string Name{
            get{
                return String.Format("{0} {1}", Material.DisplayName(), Type.DisplayName());
            }
        }

        private ITooltip<BaseItem> _tooltip;
        public ITooltip<BaseItem> Tooltip { get; private set; }

        public BaseItem() {
            Stats = new List<StatBonus>();
        }

        public override string ToString(){ 
            return String.Format("{0}\nRequired Level: {1}\n{2}", Name, RequiredLevel, String.Join("\n", Stats.Select(x => x.ToString()).ToArray()));
        }
    }
}