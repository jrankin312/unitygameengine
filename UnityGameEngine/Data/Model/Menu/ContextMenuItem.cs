﻿using System;

namespace UnityGameEngine.Data.Model{
    public class ContextMenuItem<T>{
        public bool Enabled { get; protected set; }
        public Action<T> Action { get; protected set; }
        public string Name { get; protected set; }
    }
}