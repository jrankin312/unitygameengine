﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace UnityGameEngine.Data.Model.Menu{
    public interface IHasMenu<T> {
         IList<IMenuItem<T>> MenuItems { get; set; } 
    }
}