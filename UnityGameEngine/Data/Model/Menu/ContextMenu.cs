﻿using System.Collections.Generic;

namespace UnityGameEngine.Data.Model{
    public class ContextMenu <T>{
        public T Parent { get; set; }
        public List<ContextMenuItem<T>> MenuItems { get; set; }
    }
}