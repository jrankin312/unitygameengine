﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace UnityGameEngine.Data.Model.Menu{
    public interface IMenuItem<T>{
        string DisplayText(T data);
        void Click(T data);

        event Action<T> OnClick;
    }
}