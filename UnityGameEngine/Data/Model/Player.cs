﻿using System.Collections;
using System.Collections.Generic;

namespace UnityGameEngine.Data.Model {
    public class Player{
        public IList<BaseCharacter> Characters { get; private set; }
        public IPlayerGroup Group { get; set; }

        public Player(){
            Characters = new List<BaseCharacter>();
        }
    }
}