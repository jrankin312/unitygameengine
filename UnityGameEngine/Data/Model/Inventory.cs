﻿using System.Collections.Generic;
using UnityGameEngine.Data.Model.Item;

namespace UnityGameEngine.Data.Model{
    public class Inventory : IItemContainer{
        public List<BaseItem> Items { get; set; }
        public BaseCharacter Parent { get; set; }

        public Inventory(){
            Items = new List<BaseItem>();
        }

        public Inventory(BaseCharacter parent) : this(){
            Parent = parent;
        }
    }
}