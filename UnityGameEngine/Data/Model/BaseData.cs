﻿using System;

namespace UnityGameEngine.Data.Model {
    [Serializable]
    public class BaseData{
        public int Id { get; set; }
    }
}