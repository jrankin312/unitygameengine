﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace UnityGameEngine.Data.Model.Enum{
    public abstract class EnumEx<T> : IComparable<EnumEx<int>> where T : struct{
        // holds the actual value
        protected T _t;

        // default constructor
        protected EnumEx() {
            _t = default(T);
        }

        // constructor that takes the enum value
        protected EnumEx(T t) {
            _t = t;
        }

        // performs an implicit conversion to the underlying value
        public static implicit operator T(EnumEx<T> obj) {
            return obj._t;
        }



        // parses a string to the specified type, returns null if no string match is defined
        // this is a case-sensitive version
        public static object Parse(string s, Type type) {
            FieldInfo[] fis = type.GetFields(BindingFlags.Static | BindingFlags.Public);
            foreach (FieldInfo fi in fis) {
                if (s.Equals(fi.Name)) {
                    object obj = fi.GetValue(null);
                    return obj;
                }
            }
            return null;
        }

        // System.Object overrides
        public override int GetHashCode() {
            return _t.GetHashCode();
        }

        private sealed class TEqualityComparer : IEqualityComparer<EnumEx<T>>{
            public bool Equals(EnumEx<T> x, EnumEx<T> y){
                if (ReferenceEquals(x, y)){
                    return true;
                }
                if (ReferenceEquals(x, null)){
                    return false;
                }
                if (ReferenceEquals(y, null)){
                    return false;
                }
                if (x.GetType() != y.GetType()){
                    return false;
                }
                return x._t.Equals(y._t);
            }

            public int GetHashCode(EnumEx<T> obj){
                return obj._t.GetHashCode();
            }
        }

        private static readonly IEqualityComparer<EnumEx<T>> TComparerInstance = new TEqualityComparer();

        public static IEqualityComparer<EnumEx<T>> TComparer{
            get { return TComparerInstance; }
        }

        protected bool Equals(EnumEx<T> other){
            return _t.Equals(other._t);
        }

        public override bool Equals(object obj){
            if (ReferenceEquals(null, obj)){
                return false;
            }
            if (ReferenceEquals(this, obj)){
                return true;
            }
            var other = obj as EnumEx<T>;
            return other != null && Equals(other);
        }

        public static bool operator ==(EnumEx<T> left, EnumEx<T> right){
            return Equals(left, right);
        }

        public static bool operator !=(EnumEx<T> left, EnumEx<T> right){
            return !Equals(left, right);
        }


        public int CompareTo(EnumEx<int> other){
            var tVal = _t as int?;
            var otherVal = other._t as int?;
            if (tVal.HasValue && otherVal.HasValue){
                return tVal.Value.CompareTo(otherVal.Value);
            }
            return 0;
        }

        public override string ToString() {
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo fi in fis) {
                object obj = fi.GetValue(this);
                T t = ((EnumEx<T>)obj)._t;

                if (_t.Equals(t)) {
                    var displayAtt = GetAttribute<DisplayAttribute>(fi);
                    if (displayAtt != null){
                        return displayAtt.Name;
                    }
                    return fi.Name;
                }
            }
            return string.Empty;
        }

        public string FieldName(){
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo fi in fis) {
                object obj = fi.GetValue(this);
                T t = ((EnumEx<T>)obj)._t;

                if (_t.Equals(t)) {
                    return fi.Name;
                }
            }
            return string.Empty;
        }

        public string DisplayName(){
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo fi in fis) {
                object obj = fi.GetValue(this);
                T t = ((EnumEx<T>)obj)._t;

                if (_t.Equals(t)) {
                    var displayAtt = GetAttribute<DisplayAttribute>(fi);
                    if (displayAtt != null) {
                        return displayAtt.Name;
                    }
                    return fi.Name;
                }
            }
            return string.Empty;
        }

        public string DisplayDescription(){
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public);

            foreach (FieldInfo fi in fis) {
                object obj = fi.GetValue(this);
                T t = ((EnumEx<T>)obj)._t;

                if (_t.Equals(t)) {
                    var displayAtt = GetAttribute<DisplayAttribute>(fi);
                    if (displayAtt != null) {
                        return displayAtt.Description;
                    }
                }
            }
            return string.Empty;
        }

        private static T2 GetAttribute<T2>(FieldInfo fi){
            return (T2) fi.GetCustomAttributes(typeof (T2), false).FirstOrDefault();
        }

    }
}