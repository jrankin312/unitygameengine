﻿namespace UnityGameEngine.Data.Model.Enum{
    public class BaseCharacterClass : EnumEx<int>{
        protected BaseCharacterClass() : base() {}

        protected BaseCharacterClass(int value) : base(value) {}

        public static implicit operator BaseCharacterClass(int i) {
            return new BaseCharacterClass(i);
        }
    }
}