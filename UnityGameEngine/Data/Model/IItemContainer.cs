﻿using System.Collections.Generic;
using UnityGameEngine.Data.Model.Item;

namespace UnityGameEngine.Data.Model {
    public interface IContainer<T>{
        List<T> Items { get; set; }
    }

    public interface IItemContainer : IContainer<BaseItem> {

    }
}