﻿namespace UnityGameEngine.Data.Model{
    public interface IHasTooltip<T> {
         ITooltip<T> Tooltip { get; }
    }
}