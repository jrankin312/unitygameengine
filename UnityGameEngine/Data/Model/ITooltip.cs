﻿namespace UnityGameEngine.Data.Model{
    public interface ITooltip<T> {
        T Parent { get; }
        string Title { get; set; }
        string Body { get; set; }
        bool IsVisible { get; set; }
    }
}