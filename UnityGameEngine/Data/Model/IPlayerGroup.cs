﻿using System.Collections;
using System.Collections.Generic;

namespace UnityGameEngine.Data.Model{
    public interface IPlayerGroup{
        IList<Player> Players { get; set; }
    }
}