﻿using System;

namespace UnityGameEngine.Data.Model{
    public interface ICanDie<T>{
        event Action<T> OnDeath;

        void Die(); 
    }
}