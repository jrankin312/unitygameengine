﻿using System.Collections.Generic;

namespace UnityGameEngine.Data.Model.Scene{
    public interface IScene {
        IList<Player> Players { get; set; }
        IList<IItemContainer> ItemDrops { get; set; }
        IList<BaseMonster> Enemies { get; set; }

        void SpawnEnemy();
        void SpawnEnemies();

    }
}