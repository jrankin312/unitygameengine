﻿using System;
using System.Collections.Generic;
using UnityGameEngine.Data.Model.Combat;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Data.Model {
    public class BaseMonster : BaseData, IHasStats, ICanDie<BaseMonster>, IAttacker, IAttackable {
        public IList<BaseStat> Stats { get; set; }
        public event Action<BaseMonster> OnDeath;
        public int Level { get; set; }
        public IAttack BasicAttack { get; set; }

        public void Die(){
            OnDeath(this);
        }

        public BaseMonster(){

            //make onDeath drop items and such...
        }

        public void Attack(IAttackable target) {
            if (BasicAttack != null) {
                BasicAttack.Attack(this, target);
            }
        }
    }
}