﻿using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Data.Model.Combat{
    public interface IAttacker : IHasStats {
        void Attack(IAttackable target);
    }
}