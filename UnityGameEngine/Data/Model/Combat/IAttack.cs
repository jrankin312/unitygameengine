﻿namespace UnityGameEngine.Data.Model.Combat{
    public interface IAttack {
        void Attack(IAttacker attacker, IAttackable target);
    }
}