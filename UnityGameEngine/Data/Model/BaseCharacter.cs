﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityGameEngine.Data.Model.Combat;
using UnityGameEngine.Data.Model.Enum;
using UnityGameEngine.Data.Model.Factory;
using UnityGameEngine.Data.Model.Item;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Data.Model {
    public class BaseCharacter : BaseData, ICanDie<BaseCharacter>, IHasStats, IAttackable, IAttacker {
        public string Name { get; set; }
        public Player Parent { get; set; }
        public Inventory Inventory { get; set; }
        public IDictionary<BaseItemType, BaseItem> EquippedItems { get; set; }
        public BaseCharacterClass Class { get; set; }
        public IList<BaseStat> Stats { get; set; }
        

        public IAttack BasicAttack { get; set; }
        

        public int Level { get; set; }

        public event Action<BaseCharacter> OnDeath;
        public void Die() {
            OnDeath(this);
        }


        public BaseCharacter(){
            Inventory = new Inventory(this);
            EquippedItems = new Dictionary<BaseItemType, BaseItem>();
        }

        public BaseCharacter(Player parent, BaseCharacterClass charClass, ICharacterClassFactory factory) : this(){
            Parent = parent;
            Class = charClass;
            Stats = factory.DefaultBaseStats(this);
            parent.Characters.Add(this);
        }

        /// <summary>
        /// Basically go through the Item's stat bonuses and add them to the current list
        /// of bonuses on the applicable stat.
        /// 
        /// Todo: add the item to inventory if one is already equipped.
        /// Todo: figure out a way to limit equipping items to only 1 weapon.
        /// </summary>
        /// <param name="item"></param>
        public void EquipOrConsumeItem(BaseItem item){
            if (item.IsEquippable){
                if (EquippedItems.ContainsKey(item.Type)) {
                    //remove item...
                }
                foreach (var itemStatBonus in item.Stats) {
                    var cStat = Stats.FirstOrDefault(x => x.StatType == itemStatBonus.StatType);
                    if (cStat == null) {
                        cStat = new BaseStat(itemStatBonus.StatType, StatDisplayType.Integer, 0, this);
                        Stats.Add(cStat);
                    }

                    cStat.AddBonus(itemStatBonus);
                }

                EquippedItems[item.Type] = item;
            } else if (item.IsConsumable){
                foreach (var mod in item.StatModifiers){
                    mod.ApplyTo(this);
                }
            }
        }

        public void UnequipItem(BaseItemType itemType){
            var item = EquippedItems[itemType];
            if (item == null){
                return;
            }

            foreach (var statBonus in item.Stats){
                var cStat = Stats.FirstOrDefault(x => x.StatType == statBonus.StatType);//current stat
                if (cStat == null){
                    //this should basically never happen(?)
                }
                var modified = false;
                foreach (var cStatBonus in cStat.Bonuses){
                    if (cStatBonus == statBonus){
                        cStatBonus.Value -= statBonus.Value;
                        modified = true;
                    }
                }

                foreach (var _bonus in cStat.Bonuses.ToList()){
                    if (_bonus.Value == 0){
                        //this might be sketchy if the item has more than one of the exact
                        //same stat - but that shouldn't happen...
                        cStat.Bonuses.Remove(_bonus);
                    }
                }

                if (!modified){
                    //stat bonus not found?
                }

                double TOLERANCE = .001;

                if (Math.Abs(cStat.BaseValue) < TOLERANCE && !cStat.Bonuses.Any()){
                    Stats.Remove(cStat);
                }
            }
        }

        public void Attack(IAttackable target) {
            if (BasicAttack != null) {
                BasicAttack.Attack(this, target);
            }
        }

    }
}