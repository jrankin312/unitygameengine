﻿using System.Collections.Generic;
using UnityGameEngine.Data.Model.Enum;
using UnityGameEngine.Data.Model.Item;

namespace UnityGameEngine.Data.Model.Factory{
    public interface IItemFactory{
        /// <summary>
        /// Get the list of items dropped when BaseMonster is killed.
        /// </summary>
        /// <param name="monster">BaseMonster killed.</param>
        /// <returns>List of drops from the BaseMonster.</returns>
        IItemContainer GetMonsterDropOnKill(BaseCharacter killedBy, BaseMonster monster);

        IList<BaseItemType> GetCategoryItemTypes(BaseItemCategory category);
        BaseItemCategory GetItemCategory(BaseItemType type);

        IList<BaseItemMaterial> GetPossibleItemMaterials(BaseItemType itemType);

        string GetMaterialTierName(BaseItemMaterial itemMaterial, int quality, BaseItemType itemType, int requiredLevel);

        IList<BaseCharacterClass> GetUsableClasses(BaseItem item);
        IList<BaseCharacterClass> GetUsableClasses( BaseItemMaterial material, BaseItemType type);

    }
}