﻿using System.Collections.Generic;
using UnityGameEngine.Data.Model.Enum;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Data.Model.Factory {
    public interface ICharacterClassFactory {
        IList<BaseStat> DefaultBaseStats(BaseCharacter character);
    }
}