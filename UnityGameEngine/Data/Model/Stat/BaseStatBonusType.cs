﻿using UnityGameEngine.Data.Model.Enum;

namespace UnityGameEngine.Data.Model.Stat{
    public class BaseStatBonusType : EnumEx<int>{
        protected BaseStatBonusType() : base() {}

        protected BaseStatBonusType(int value) : base(value){
        }

        public static implicit operator BaseStatBonusType(int i){
            return new BaseStatBonusType(i);
        }

        public static BaseStatBonusType EquippedItem = 1;
        public static BaseStatBonusType Temporary = 2;
        public static BaseStatBonusType Permanent = 3;
        public static BaseStatBonusType Buff = 4;
        public static BaseStatBonusType Debuff = 5;
    }
}