﻿using System;
using System.Collections;
using UnityEngine;
using UnityGameEngine.Unity;

namespace UnityGameEngine.Data.Model.Stat{
    public class BaseStatModifier {
        public IHasStats StatsAppliedTo { get; private set; }
        public StatBonus BonusPerTick { get; private set; }
        public int NumTicks { get; private set; }
        public double TickDuration { get; private set; }

        
        public DateTime? Started { get; set; }
        private Task Task { get; set; }

        private int CurrentTick { get; set; }

        public double DurationRemaining{
            get{
                if (!Started.HasValue){
                    return 0;
                } else{
                    var started = Started.Value;
                    var timeSpan = DateTime.Now - started;
                    return Math.Max(0, timeSpan.TotalSeconds - NumTicks*TickDuration);
                }
            }
        }

        public BaseStatModifier(StatBonus bonusPerTick, int numTicks, double tickDuration){
            BonusPerTick = bonusPerTick;
            NumTicks = numTicks;
            TickDuration = tickDuration;
        }

        public void ApplyTo(IHasStats stats){
            StatsAppliedTo = stats;
            Started = DateTime.Now;
            Task = new Task(ModifyStats());
        }

        private IEnumerator ModifyStats(){
            BaseStat stat = null;
            for (CurrentTick = 0; CurrentTick < NumTicks; CurrentTick++){
                stat = StatsAppliedTo.Stats.Get(BonusPerTick.StatType);
                if (stat != null){
                    stat.AddBonus(BonusPerTick);
                }
                yield return new WaitForSeconds((float)TickDuration);
            }

            if (stat != null){
                stat.RemoveBonus(BonusPerTick * NumTicks);
            }
        }

    }
}