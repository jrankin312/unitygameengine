﻿using UnityGameEngine.Data.Model.Enum;

namespace UnityGameEngine.Data.Model.Stat{
    public class BaseStatType : EnumEx<int>{
        protected BaseStatType() : base() {}

        protected BaseStatType(int value) : base(value){
        }

        public static implicit operator BaseStatType(int i){
            return new BaseStatType(i);
        }

        public static BaseStatType Experience = 999;
    }
}