﻿using System;

namespace UnityGameEngine.Data.Model.Stat{
    public class StatBonus : IEquatable<StatBonus>, ICloneable{
        public BaseStatType StatType { get; set; }
        public BaseStatBonusType BonusType { get; set; }
        public bool IsPercentage { get; set; }
        public double Value { get; set; }
        public int Priority { get; private set; }

        public StatBonus(){
            
        }

        public object Clone(){
            return new StatBonus(this);
        }

        public StatBonus(StatBonus bonus){
            StatType = bonus.StatType;
            BonusType = bonus.BonusType;
            IsPercentage = bonus.IsPercentage;
            Value = bonus.Value;
            Priority = bonus.Priority;
        }

        public StatBonus(BaseStatType statType, BaseStatBonusType bonusType, double value, bool isPercentage) : this(){
            StatType = statType;
            BonusType = bonusType;
            Value = value;
            IsPercentage = isPercentage;
            Priority = 10;
        }

        public StatBonus(BaseStatType statType, BaseStatBonusType bonusType, double value, bool isPercentage,  int priority){
            BonusType = bonusType;
            StatType = statType;
            IsPercentage = isPercentage;
            Value = value;
            Priority = priority;
        }


        public bool Equals(StatBonus other){
            if (ReferenceEquals(null, other)){
                return false;
            }
            if (ReferenceEquals(this, other)){
                return true;
            }
            return Equals(StatType, other.StatType) && Equals(BonusType, other.BonusType) && IsPercentage.Equals(other.IsPercentage) && Priority == other.Priority;
        }

        public override bool Equals(object obj){
            if (ReferenceEquals(null, obj)){
                return false;
            }
            if (ReferenceEquals(this, obj)){
                return true;
            }
            var other = obj as StatBonus;
            return other != null && Equals(other);
        }

        public override int GetHashCode(){
            unchecked{
                int hashCode = (StatType != null ? StatType.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (BonusType != null ? BonusType.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ IsPercentage.GetHashCode();
                hashCode = (hashCode*397) ^ Priority;
                return hashCode;
            }
        }

        public static StatBonus operator *(StatBonus left, int right){
            var bonus = new StatBonus(left);
            if (bonus.IsPercentage){
                bonus.Value = (bonus.Value - 1)*right + 1;
            } else{
                bonus.Value *= right;
            }
            return bonus;
        }

        public static bool operator ==(StatBonus left, StatBonus right){
            return Equals(left, right);
        }

        public static bool operator !=(StatBonus left, StatBonus right){
            return !Equals(left, right);
        }

        public override string ToString(){
            if (IsPercentage){
                return String.Format("{0}{1:0.##%} {2}", Value-1 >= 0 ? "+" : "", Value-1, StatType);
            } else{
                return String.Format("{0}{1} {2}", Value >= 0 ? "+" : "", Value, StatType);
            }
        }
    }
}