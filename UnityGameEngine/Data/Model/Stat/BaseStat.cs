﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.AccessControl;

namespace UnityGameEngine.Data.Model.Stat{
    /// <summary>
    /// 
    /// </summary>
    public class BaseStat{
        protected const double TOLERANCE = .001;

        public BaseData Parent { get; set; }

        public BaseStatType StatType { get; set; }

        public StatDisplayType DisplayType { get; set; }

        public IList<StatBonus> Bonuses { get; private set; }

        private double _baseValue;

        public double BaseValue{
            get { return _baseValue; }
            set{
                if (!_baseValue.Equals(value)){
                    _baseValue = value;
                    if (BaseValueChanged != null){
                        BaseValueChanged(this);
                    }
                }
            }
        }

        public double CurrentValue{
            get{
                var val = BaseValue;
                foreach (var bonus in Bonuses.OrderBy(x => x.Priority)){
                    if (bonus.IsPercentage){
                        val *= bonus.Value;
                    } else{
                        val += bonus.Value;
                    }
                }
                return val;
            }
            set{
                var cVal = CurrentValue;
                if (Math.Abs(cVal - value) > TOLERANCE){
                    var addVal = value - CurrentValue;
                    var bonus = new StatBonus(StatType, BaseStatBonusType.Temporary, addVal, false, 20);
                    AddBonus(bonus);
                }
            }
        }

        public double MaxValue{
            get{
                var val = BaseValue;
                foreach (
                    var bonus in Bonuses.Where(x => x.BonusType != BaseStatBonusType.Temporary).OrderBy(x => x.Priority)
                    ){
                    if (bonus.IsPercentage){
                        val *= bonus.Value;
                    } else{
                        val += bonus.Value;
                    }
                }
                return val;
            }
        }


        public event Action<BaseStat> BaseValueChanged;
        public event Action<BaseStat> CurrentValueChanged;

        public BaseStat(){
            Bonuses = new List<StatBonus>();
        }

        protected BaseStat(BaseStat stat){
            StatType = stat.StatType;
            _baseValue = stat.BaseValue;
            Parent = stat.Parent;
        }

        public BaseStat(BaseStatType statType, StatDisplayType displayType, double baseValue, BaseData parent) : this(){
            StatType = statType;
            _baseValue = baseValue;
            DisplayType = displayType;
            Parent = parent;
        }

        public void AddBonus(StatBonus bonus){
            StatBonus cBonus =
                Bonuses.FirstOrDefault(
                    x => x == bonus);

            if (cBonus != null){
                if (bonus.IsPercentage){
                    cBonus.Value *= bonus.Value;
                } else{
                    cBonus.Value += bonus.Value;
                }
            } else{
                Bonuses.Add(bonus);
            }

            if (CurrentValueChanged != null){
                CurrentValueChanged(this);
            }
        }

        public void RemoveBonus(StatBonus bonus, bool removeTemporary = false){
            if (!removeTemporary && bonus.BonusType == BaseStatBonusType.Temporary){
                return;
            }

            StatBonus cBonus = Bonuses.FirstOrDefault(x => x == bonus);
            if (cBonus != null){
                if (bonus.IsPercentage){
                    cBonus.Value *= 1/bonus.Value;
                } else{
                    cBonus.Value -= bonus.Value;
                }
            } else{
                //do nothing? throw an exception? no idea...
            }

            if (CurrentValueChanged != null){
                CurrentValueChanged(this);
            }
        }

        protected bool Equals(BaseStat other){
            return StatType == other.StatType;
        }

        public override bool Equals(object obj){
            if (ReferenceEquals(null, obj)){
                return false;
            }
            if (ReferenceEquals(this, obj)){
                return true;
            }
            if (obj.GetType() != this.GetType()){
                return false;
            }
            return Equals((BaseStat) obj);
        }

        public override int GetHashCode(){
            return (StatType != null ? StatType.GetHashCode() : 0);
        }

        public static bool operator ==(BaseStat left, BaseStat right){
            return Equals(left, right);
        }

        public static bool operator !=(BaseStat left, BaseStat right){
            return !Equals(left, right);
        }

        public override string ToString(){
            switch (DisplayType){
                case StatDisplayType.Integer:
                    /****************
                    * Int Stat
                    *****************/
                    /*
                    if (IsModification){
                        if (BaseValue == 0 && MaxValue == 0){
                            return "";
                        }
                        if (BaseValue != MaxValue){
                            return String.Format("{0}{1}/{2} to {3}", BaseValue >= 0 ? "+" : "-", BaseValue,
                                MaxValue, StatType);
                        }
                        return String.Format("{0}{1} to {2}", BaseValue >= 0 ? "+" : "-", BaseValue, StatType);
                    }


                    if (BaseValue != MaxValue){
                        return String.Format("{0}/{1} {2}", BaseValue, MaxValue, StatType);
                    }
                     */
                    return String.Format("{0} {1}", CurrentValue, StatType.DisplayName());
                    break;
                case StatDisplayType.Double:
                    /****************
                    * Double Stat
                    *****************/
                    /*
                    if (IsModification){
                        if (Math.Abs(BaseValue) < TOLERANCE && Math.Abs(MaxValue) < TOLERANCE){
                            return "";
                        }
                        if (IsPercentage){
                            return String.Format("{0}{1:0.##%} to {2}", BaseValue >= 0 ? "+" : "-", BaseValue,
                                StatType);
                        }
                        if (Math.Abs(BaseValue - MaxValue) > TOLERANCE){
                            return String.Format("{0}{1:0.##}/{2:0.##} to {3}", BaseValue >= 0 ? "+" : "-",
                                BaseValue,
                                MaxValue, StatType);
                        }
                        return String.Format("+{0:0.##} to {1}", BaseValue, StatType);
                    }


                    if (IsPercentage){
                        return String.Format("{0}{1:0.##%} to {2}", BaseValue >= 0 ? "" : "-", BaseValue, StatType);
                    }
                    if (Math.Abs(BaseValue - MaxValue) > TOLERANCE){
                        return String.Format("{0}{1:0.##}/{2:0.##} to {3}", BaseValue >= 0 ? "" : "-", BaseValue,
                            MaxValue,
                            StatType);
                    }
                     */
                    return String.Format("+{0:0.##} to {1}", CurrentValue, StatType.DisplayName());
                    break;
                case StatDisplayType.Damage:
                    /****************
                    * Damage Stat
                    *****************/
                    /*
                    if (IsModification){
                        if (BaseValue == 0 && MaxValue == 0){
                            return "";
                        }
                        if (BaseValue != MaxValue){
                            return String.Format("{0}{1}-{2} {3}", BaseValue >= 0 ? "+" : "-", BaseValue, MaxValue,
                                StatType);
                        }
                        return String.Format("{0}{1} {2}", BaseValue >= 0 ? "+" : "-", BaseValue, StatType);
                    }


                    if (BaseValue != MaxValue){
                        return String.Format("{0}-{1} {2}", BaseValue, MaxValue, StatType);
                    }
                     */
                    return String.Format("{0} {1}", CurrentValue, StatType.DisplayName());
                    break;
            }

            return "";
        }
    }

    public enum StatDisplayType{
        Integer,
        Double,
        Damage
    }
}