﻿namespace UnityGameEngine.Data.Model.Stat{
    public class BaseDamageStatType : BaseStatType {

        protected BaseDamageStatType() : base() { }

        protected BaseDamageStatType(int value)
            : base(value) {
        }

        public static implicit operator BaseDamageStatType(int i) {
            return new BaseDamageStatType(i);
        }

    }
}