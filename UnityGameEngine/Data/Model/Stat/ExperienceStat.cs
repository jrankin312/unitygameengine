﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityGameEngine.Data.Model.Stat{
    public class ExperienceStat : BaseStat {
        public BaseStat Experience { get; private set; }

        public int CurrentLevel { get; private set; }
        
        protected ExperienceStat(int level, BaseStat stat) : base(stat){
            Setup(level);
        }

        public ExperienceStat(int level, BaseStatType statType, StatDisplayType displayType, double baseValue, BaseData parent) : base(statType, displayType, baseValue,  parent){
            Setup(level);
        }

        private void Setup(int level){
            Experience = new BaseStat(BaseStatType.Experience, StatDisplayType.Integer, 0, Parent);

            CurrentLevel = level-1;
            LevelUp();

            Experience.CurrentValueChanged += exp => {
                //if (exp.BaseValue >= exp.MaxValue){
                //    LevelUp();
                //}
            };
        }

        public event Action<ExperienceStat> OnLevelUp;
        private void LevelUp(){
            CurrentLevel += 1;

            //Experience.BaseValue = Math.Max(0, Experience.MaxValue - Experience.BaseValue);
            //Experience.MaxValue = Math.Floor(Enumerable.Range(1, CurrentLevel - 1).Sum(x => Math.Floor(x + 300 * Math.Pow(2, (double)x / 7))) / 4);
            
            if (OnLevelUp != null){
                OnLevelUp(this);
            }
        }

    }
}