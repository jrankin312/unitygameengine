﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UnityGameEngine.Data.Model.Stat{
    public static class StatExtensions {
        public static BaseStat Get(this IList<BaseStat> stats, BaseStatType type) {
            return stats.FirstOrDefault(x => x.StatType == type);
        }
    }
}