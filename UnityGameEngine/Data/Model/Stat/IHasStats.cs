﻿using System.Collections.Generic;

namespace UnityGameEngine.Data.Model.Stat {
    public interface IHasStats {
        IList<BaseStat> Stats { get; }
    }
}