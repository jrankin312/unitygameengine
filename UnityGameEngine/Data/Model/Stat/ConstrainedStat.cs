﻿using System;

namespace UnityGameEngine.Data.Model.Stat{
    public class ConstrainedStat : BaseStat {

        public ConstrainedStat(BaseStatType statType, StatDisplayType displayType, double baseValue, BaseData parent) : base(statType, displayType, baseValue, parent){
            CurrentValueChanged += stat => {
                stat.CurrentValue = Math.Min(Math.Max(stat.BaseValue, 0), MaxValue);
            };
        }
    }
}