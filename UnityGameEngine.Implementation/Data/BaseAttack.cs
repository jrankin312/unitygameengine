﻿using System;
using System.Linq;
using UnityGameEngine.Data.Model.Combat;
using UnityGameEngine.Data.Model.Stat;
using UnityGameEngine.Implementation.Data.Model;

namespace UnityGameEngine.Implementation.Data{
    public class BaseAttack : IAttack {
        public DateTime? LastUsed { get; set; }
        public double Cooldown { get; set; }

        public BaseAttack(){
            Cooldown = 1;
        }

        public void Attack(IAttacker attacker, IAttackable target){
            if (!LastUsed.HasValue || (DateTime.Now - LastUsed.Value).TotalMilliseconds > Cooldown*1000){
                foreach (var dmgStat in attacker.Stats.Where(x => x.StatType as BaseDamageStatType != null)){
                    target.Stats.Get(StatType.Life).CurrentValue -= dmgStat.CurrentValue;
                }
            }

            LastUsed = DateTime.Now;
        }
    }
}