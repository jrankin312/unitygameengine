﻿using System.ComponentModel.DataAnnotations;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Implementation.Data.Model{
    public class StatType : BaseStatType {
        protected StatType() : base(){}

        protected StatType(int value) : base(value) {
        }

        public static implicit operator StatType(int i) {
            return new StatType(i);
        }

        public static StatType Level = 1;

        public static StatType Life = 2;
        public static StatType Mana = 3;
        
        public static StatType Experience = 4;

        [Display(Name = "Life Regen")]
        public static StatType LifeRegen = 5;
        [Display(Name = "Mana Regen")]
        public static StatType ManaRegen = 6;

        public static StatType Dexterity = 7;
        public static StatType Strength = 8;
        public static StatType Vitality = 9;
        public static StatType Intelligence = 10;

        [Display(Name = "Experience Gained")]
        public static StatType ExperienceGained = 11;

        public static DamageStatType PhysicalDamage = 12;
        public static DamageStatType FireDamage = 13;
        public static DamageStatType ColdDamage = 14;
    }
}