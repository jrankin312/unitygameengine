﻿using System.Collections.Generic;
using UnityGameEngine.Data.Model;
using UnityGameEngine.Data.Model.Factory;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Implementation.Data.Model.Factory{
    public class CharacterClassFactory : ICharacterClassFactory{
        public IList<BaseStat> DefaultBaseStats(BaseCharacter character){
            if (character.Class == CharacterClass.Barbarian){
                return new List<BaseStat>{
                    new ConstrainedStat(StatType.Life, StatDisplayType.Integer, 100,  null),
                    new ConstrainedStat(StatType.Mana, StatDisplayType.Integer, 100,  null),
                    new BaseStat(StatType.Strength, StatDisplayType.Integer, 60, null),
                    new BaseStat(StatType.Dexterity, StatDisplayType.Integer, 25, null),
                    new BaseStat(StatType.Intelligence, StatDisplayType.Integer, 5,  null),
                    new BaseStat(StatType.Vitality, StatDisplayType.Integer, 60, null),
                };
            }
            if (character.Class == CharacterClass.Fighter){
                return new List<BaseStat>{
                    new ConstrainedStat(StatType.Life, StatDisplayType.Integer, 100, null),
                    new ConstrainedStat(StatType.Mana, StatDisplayType.Integer, 100, null),
                    new BaseStat(StatType.Strength, StatDisplayType.Integer, 50, null),
                    new BaseStat(StatType.Dexterity, StatDisplayType.Integer, 40, null),
                    new BaseStat(StatType.Intelligence, StatDisplayType.Integer, 10, null),
                    new BaseStat(StatType.Vitality, StatDisplayType.Integer, 50, null),
                };
            }
            return new List<BaseStat>();
        }
    }
}