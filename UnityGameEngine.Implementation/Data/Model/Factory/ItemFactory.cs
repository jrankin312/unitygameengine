﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityGameEngine.Data.Model;
using UnityGameEngine.Data.Model.Enum;
using UnityGameEngine.Data.Model.Factory;
using UnityGameEngine.Data.Model.Item;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Implementation.Data.Model.Factory {
    public class ItemFactory : IItemFactory {
        public ItemFactory() {
            
        }

        //Number of drops
        //40% no drop
        //40% one drop
        //20% two drops


        static Random _random = new Random();


        public IItemContainer GetMonsterDropOnKill(BaseCharacter killedBy, BaseMonster monster){
            var random = _random;
            var rNumDrops = random.Next(0, 5);
            if (rNumDrops < 2){
                return new ItemDrop();
            } else if (rNumDrops < 4){
                return new ItemDrop()
                    .AddItem(GetRandomItem(killedBy, monster));
            } else{
                return new ItemDrop()
                    .AddItem(GetRandomItem(killedBy, monster))
                    .AddItem(GetRandomItem(killedBy, monster));
            }
        }


        private IList<StatBonus> StatsPerQuality = new List<StatBonus>{
            new StatBonus(StatType.Life, BaseStatBonusType.EquippedItem, 100, false),
            new StatBonus(StatType.Life, BaseStatBonusType.EquippedItem, 1.02, true),

            new StatBonus(StatType.Mana, BaseStatBonusType.EquippedItem, 100, false),
            new StatBonus(StatType.Mana, BaseStatBonusType.EquippedItem, 1.02, true),

            new StatBonus(StatType.Dexterity, BaseStatBonusType.EquippedItem, 5, false),
            new StatBonus(StatType.Strength, BaseStatBonusType.EquippedItem, 5, false),
            new StatBonus(StatType.Vitality, BaseStatBonusType.EquippedItem, 5, false),
            new StatBonus(StatType.Intelligence, BaseStatBonusType.EquippedItem, 5, false),

            new StatBonus(StatType.FireDamage, BaseStatBonusType.EquippedItem, 10, false),
            new StatBonus(StatType.ColdDamage, BaseStatBonusType.EquippedItem, 10, false),
        };


        public List<StatBonus> MaxSpawnableItemStats(int quality){
            
            var spawnableStats = new List<StatBonus>();
            StatsPerQuality.ToList().ForEach(x => spawnableStats.Add((StatBonus)x.Clone()));
            spawnableStats.ForEach(x => x.Value = !x.IsPercentage ? x.Value * (quality+1) : ((x.Value-1) * (quality+1))+1);
            return spawnableStats;
        }


        private double[] numStats = {1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,4,4,4,5,5,5,6,6,7,7,8,8,9,9};
        private double[] statQuality = {1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10};

        //I'm not going to remember why I did any of this in like 3 days.
        private BaseItem GetRandomItem(BaseCharacter killedBy, BaseMonster monster){
            var random = _random;
            var maxQuality = Math.Max(0, monster.Level / 5 - 3);

            var rIsMaxQuality = random.Next(0, 5);
            int rQuality = rIsMaxQuality < 3 ? random.Next(0, maxQuality) : maxQuality;
            var rNumStats = Math.Floor((Shuffle(numStats)[0] + Shuffle(numStats)[0])/2);
            var possibleItemTypes = CategoryTypes.SelectMany(x => x.Value).ToList();
            var rItemType = possibleItemTypes[random.Next(0, possibleItemTypes.Count())];
            var possibleItemMaterials = GetPossibleItemMaterials(rItemType).ToList();
            var rItemMaterial = possibleItemMaterials[random.Next(0, possibleItemMaterials.Count())];
            
            var maxPossibleStats = MaxSpawnableItemStats(rQuality).ToList();
            var rItemStats = new List<StatBonus>();
            for (var i = 0; i < rNumStats; i++){
                var rStatQuality = (Shuffle(statQuality)[0] + Shuffle(statQuality)[0]) / 2;
                var stat = new StatBonus(maxPossibleStats[random.Next(0, maxPossibleStats.Count())]);
                if (rItemStats.Any(x => x.StatType == stat.StatType && x.IsPercentage == stat.IsPercentage)){
                    i--;
                } else{
                    if (stat.IsPercentage){
                        stat.Value = ((stat.Value-1)*rStatQuality/10)+1;
                    } else{
                        stat.Value *= rStatQuality / 10;
                    }
                    
                    stat.Value = Math.Max(!stat.IsPercentage ?1 : 1.01, Math.Round(stat.Value));
                    rItemStats.Add(stat);
                    maxPossibleStats.Remove(stat);
                }
            }

            var rItem = new BaseItem {
                Id = 0,
                Type = rItemType,
                Category = GetItemCategory(rItemType),
                IsConsumable = false, //change this later...
                IsEquippable = true,
                Material = rItemMaterial,
                Quality = rQuality,
                Stats = rItemStats,
            };
            
            return rItem;
        }

        

        /// <summary>
        /// Shuffle the array.
        /// </summary>
        /// <typeparam name="T">Array element type.</typeparam>
        /// <param name="array">Array to shuffle.</param>
        public static T[] Shuffle<T>(T[] array) {
            var random = _random;
            for (int i = array.Length; i > 1; i--) {
                // Pick random element to swap.
                int j = random.Next(i); // 0 <= j <= i-1
                // Swap.
                T tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
            return array;
        }



        private IDictionary<BaseItemCategory, IList<BaseItemType>> CategoryTypes =
            new Dictionary<BaseItemCategory, IList<BaseItemType>> {
                {
                    ItemCategory.Weapon, new List<BaseItemType> {
                        ItemType.Sword,
                        ItemType.Wand
                    }
                }, {
                    ItemCategory.Armor, new List<BaseItemType> {
                        ItemType.Helm,
                        ItemType.Chest,
                        ItemType.Gloves,
                        ItemType.Boots,
                        ItemType.Amulet,
                        ItemType.Ring
                    }
                }
            };

        public IList<BaseItemType> GetCategoryItemTypes(BaseItemCategory category) {
            return CategoryTypes[category];
        }

        public BaseItemCategory GetItemCategory(BaseItemType type) {
            foreach (var categoryType in CategoryTypes) {
                if (categoryType.Value.Contains(type)) {
                    return categoryType.Key;
                }
            }
            return null;
        }

        private IDictionary<BaseItemType, IList<BaseItemMaterial>> ItemMaterials = new Dictionary<BaseItemType, IList<BaseItemMaterial>> {
            {
                ItemType.Amulet,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal
                }
            }, {
                ItemType.Ring,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal
                }
            }, {
                ItemType.Chest,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal,
                    ItemMaterial.Leather,
                    ItemMaterial.Bone,
                    ItemMaterial.Cloth
                }
            },{
                ItemType.Helm,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal,
                    ItemMaterial.Leather,
                    ItemMaterial.Bone,
                    ItemMaterial.Cloth
                }
            },{
                ItemType.Boots,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal,
                    ItemMaterial.Leather,
                    ItemMaterial.Bone,
                    ItemMaterial.Cloth
                }
            },{
                ItemType.Gloves,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal,
                    ItemMaterial.Leather,
                    ItemMaterial.Bone,
                    ItemMaterial.Cloth
                }
            },
            
            
            {
                ItemType.Sword,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal,
                }
            },

            {
                ItemType.Wand,
                new List<BaseItemMaterial> {
                    ItemMaterial.Bone
                }
            },

        }; 

        public IList<BaseItemMaterial> GetPossibleItemMaterials(BaseItemType itemType) {
            return ItemMaterials[itemType];
        }

        public string GetMaterialTierName(BaseItemMaterial itemMaterial, int quality, BaseItemType itemType, int requiredLevel) {
            if (itemMaterial == ItemMaterial.Metal) {
                switch (quality) {
                    case 1:
                        return "Rusted";
                    case 2:
                        return "Copper";
                    case 3:
                        return "Bronze";
                    case 4:
                        return "Iron";
                    case 5:
                        return "Steel";
                    case 6:
                        return "Alloy";
                    case 7:
                        return "Fine Alloy";
                    case 8:
                        return "Mithril";
                    case 9:
                        return "Sapphire";
                    case 10:
                        return "Adamantium";
                    case 11:
                        return "Diamond";
                    case 12:
                        return "Tempered-Diamond";
                    case 13:
                        return "Tintitanium";
                    case 14:
                        return "Ultimanium";
                }
            }
            return "";
        }

        public IList<BaseCharacterClass> GetUsableClasses(BaseItem item) {
            return GetUsableClasses(item.Material, item.Type);
        }

        private IDictionary<BaseCharacterClass, IList<BaseItemMaterial>> ClassUsableMaterials = new Dictionary<BaseCharacterClass, IList<BaseItemMaterial>> {
            {
                CharacterClass.Barbarian,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal
                }
            }, {
                CharacterClass.Fighter,
                new List<BaseItemMaterial> {
                    ItemMaterial.Metal,
                    ItemMaterial.Bone
                }
            }
        };

        private IDictionary<BaseCharacterClass, IList<BaseItemCategory>> ClassUsableCategories =
            new Dictionary<BaseCharacterClass, IList<BaseItemCategory>> {
                {
                    CharacterClass.Fighter,
                    new List<BaseItemCategory> {
                        ItemCategory.Weapon
                    }
                }
            };

        private IDictionary<BaseCharacterClass, IList<BaseItemType>> ClassUsableTypes = new Dictionary<BaseCharacterClass, IList<BaseItemType>> {
            {
                CharacterClass.Barbarian,
                new List<BaseItemType> {
                    ItemType.Helm,
                    ItemType.Chest,
                    ItemType.Gloves,
                    ItemType.Boots,
                    ItemType.Amulet,
                    ItemType.Ring
                }
            }, {
                CharacterClass.Fighter,
                new List<BaseItemType> {
                    ItemType.Helm
                }
            }
        };

        /// <summary>
        /// Not sure if this is customizable enough, basically it takes the material and itemtype of an item and returns all
        /// of the classes that are able to use the specified item based on the intersection of the above dictionaries itemtypes.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IList<BaseCharacterClass> GetUsableClasses(BaseItemMaterial material, BaseItemType type) {
            var matClasses = new List<BaseCharacterClass>();
            foreach (var mat in ClassUsableMaterials) {
                if (mat.Value.Contains(material)) {
                    matClasses.Add(mat.Key);
                }
            }

            var typeClasses = new List<BaseCharacterClass>();
            foreach (var typeClass in ClassUsableTypes) {
                if (typeClass.Value.Contains(type)) {
                    typeClasses.Add(typeClass.Key);
                }
            }

            foreach (var catClass in ClassUsableCategories) {
                foreach (var category in catClass.Value) {
                    if (GetCategoryItemTypes(category).Contains(type)) {
                        typeClasses.Add(catClass.Key);
                        break;
                    }
                }
            }

            return matClasses.Distinct().Intersect(typeClasses.Distinct()).Distinct().ToList();
        }
    }
}