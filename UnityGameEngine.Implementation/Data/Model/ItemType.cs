﻿using System.ComponentModel.DataAnnotations;
using UnityGameEngine.Data.Model.Item;

namespace UnityGameEngine.Implementation.Data.Model{
    public class ItemType : BaseItemType{
        protected ItemType() : base(){
        }

        protected ItemType(int value)
            : base(value){
        }

        public static implicit operator ItemType(int i){
            return new ItemType(i);
        }

        public static ItemType Helm = 1;
        public static ItemType Chest = 2;
        public static ItemType Gloves = 3;
        public static ItemType Boots = 4;
        public static ItemType Amulet = 5;
        public static ItemType Ring = 6;

        public static ItemType Sword = 7;
        public static ItemType Axe = 8;
        public static ItemType Wand = 9;



    }
}