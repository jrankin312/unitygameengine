﻿using System.ComponentModel.DataAnnotations;
using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Implementation.Data.Model{
    public class DamageStatType : BaseDamageStatType {

        protected DamageStatType() : base(){}

        protected DamageStatType(int value) : base(value) {
        }

        public static implicit operator DamageStatType(int i) {
            return new DamageStatType(i);
        }

        [Display(Name = "Physical Damage")]
        public static DamageStatType PhysicalDamage = 12;

        [Display(Name = "Fire Damage")]
        public static DamageStatType FireDamage = 13;

        [Display(Name = "Cold Damage")]
        public static DamageStatType ColdDamage = 14;
    }
}