﻿using UnityGameEngine.Data.Model.Item;

namespace UnityGameEngine.Implementation.Data.Model {
    public class ItemCategory : BaseItemCategory{
        protected ItemCategory() : base() {
        }

        protected ItemCategory(int value)
            : base(value) {
        }

        public static implicit operator ItemCategory(int i) {
            return new ItemCategory(i);
        }

        public static ItemCategory Armor = 1;
        public static ItemCategory Weapon = 2;
    }
}