﻿using UnityGameEngine.Data.Model.Stat;

namespace UnityGameEngine.Implementation.Data.Model{
    public class DamageType : BaseDamageStatType {
        protected DamageType() : base(){
        }

        protected DamageType(int value)
            : base(value){
        }

        public static implicit operator DamageType(int i){
            return new DamageType(i);
        }

        public static DamageType Physical = 1;
        public static DamageType Fire = 2;
        public static DamageType Lightning = 3;
        public static DamageType Cold = 4;

    }
}