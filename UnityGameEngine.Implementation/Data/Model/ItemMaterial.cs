﻿using UnityGameEngine.Data.Model.Item;

namespace UnityGameEngine.Implementation.Data.Model {
    public class ItemMaterial : BaseItemMaterial {
        protected ItemMaterial() : base() {
        }

        protected ItemMaterial(int value) : base(value) {
        }

        public static implicit operator ItemMaterial(int i){
            return new ItemMaterial(i);
        }

        public static ItemMaterial Metal = 1;
        public static ItemMaterial Wood = 2;
        public static ItemMaterial Leather = 3;
        public static ItemMaterial Cloth = 4;
        public static ItemMaterial Bone = 5;
        public static ItemMaterial Numeral = 6;
        public static ItemMaterial Fish = 7;
    }
}