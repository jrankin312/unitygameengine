﻿using UnityGameEngine.Data.Model.Enum;

namespace UnityGameEngine.Implementation.Data.Model {
    public class CharacterClass : BaseCharacterClass {
        protected CharacterClass() : base(){
        }

        protected CharacterClass(int value)
            : base(value){
        }

        public static implicit operator CharacterClass(int i) {
            return new CharacterClass(i);
        }

        public static CharacterClass Barbarian = 1;
        public static CharacterClass Fighter = 2;
    }
}