﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityGameEngine.Data.Model;
using UnityGameEngine.Data.Model.Item;
using UnityGameEngine.Data.Model.Stat;
using UnityGameEngine.Implementation.Data;
using UnityGameEngine.Implementation.Data.Model;
using UnityGameEngine.Implementation.Data.Model.Factory;

namespace ConsoleTests {
    class Program {
        static void Main(string[] args){
            var stat = new BaseStat(StatType.Life, StatDisplayType.Integer, 100, null);
            //var stat2 = new BaseStat(StatType.PhysicalDamage, StatDisplayType.Damage, 1, 10, true, false, null);
            stat.CurrentValueChanged += data => Console.WriteLine("Stat changed: " + data);

            stat.BaseValue = 101;

            Console.WriteLine(stat);

            Console.WriteLine(StatType.LifeRegen.ToString());
            Console.WriteLine(StatType.Experience.ToString());
            Console.WriteLine(StatType.ExperienceGained.ToString());

            var itemFactory = new ItemFactory();

            var category = itemFactory.GetItemCategory(ItemType.Wand);
            Console.WriteLine(category.DisplayName());

            var material = ItemMaterial.Metal;
            var type = ItemType.Sword;
            var usableClasses = itemFactory.GetUsableClasses(material, type);

            foreach (var charClass in usableClasses) {
                Console.WriteLine(charClass + " can use " + material + " " + type);
            }

            var statType = StatType.FireDamage;
            var stat3 = new BaseStat(statType, StatDisplayType.Damage, 10,  null);
            var stat4 = new BaseStat(statType, StatDisplayType.Damage, 10,  null);
            var stat5 = new BaseStat(statType, StatDisplayType.Damage, 20, null);

            var character = new BaseCharacter(){
                Level = 50,
                Stats = new List<BaseStat>{
                    new BaseStat(StatType.Life, StatDisplayType.Integer, 100, null),
                    new BaseStat(StatType.Mana, StatDisplayType.Integer, 100, null),
                },
            };

            var EquippedItems = new Dictionary<BaseItemType, BaseItem>{
                {
                    ItemType.Amulet,
                    new BaseItem{
                        Type = ItemType.Amulet,
                        IsEquippable = true,
                        Stats = new List<StatBonus>{
                            new StatBonus(StatType.Mana, BaseStatBonusType.EquippedItem, 110, false),
                            new StatBonus(StatType.Life, BaseStatBonusType.EquippedItem, 1.1, true),
                            new StatBonus(StatType.ColdDamage, BaseStatBonusType.EquippedItem, 10, false),
                        }
                    }
                }
            };

            character.EquipOrConsumeItem(EquippedItems[ItemType.Amulet]);

            Console.WriteLine("\nItem Stats\n---------------");
            foreach (var iStat in character.EquippedItems[ItemType.Amulet].Stats){
                Console.WriteLine(iStat);
            }

            Console.WriteLine("\nCharacter Stats\n-----------------");
            foreach (var cStat in character.Stats){
                Console.WriteLine(cStat);
            }

            character.Stats.Get(StatType.Life).CurrentValue += 10;
            character.UnequipItem(ItemType.Amulet);
            Console.WriteLine("-\nRemoved Item\nAdded 10 Life\n-");

            Console.WriteLine("\nCharacter Stats\n-----------------");
            foreach (var cStat in character.Stats) {
                Console.WriteLine(cStat);
            }


            var monster = new BaseMonster{
                Level = 20
            };

            Console.WriteLine("\n-\nItem Drops from a Level 20 monster\n-");
            //simulate random drops from killing this level 20 mob 3 times
            for (int i = 0; i < 3; i++){
                Console.WriteLine("-\nItem Drop " + (i+1) + "\n-");
                var itemDrop = itemFactory.GetMonsterDropOnKill(character, monster);
                if (itemDrop.Items.Any()) {
                    foreach (var item in itemDrop.Items) {
                        Console.WriteLine(item + "\n\n");
                    }
                } else {
                    Console.Write("No drop!");
                }
            }

            //create a new mob with 100 life and mana.
            var mob = new BaseMonster{
                Stats = new List<BaseStat>{
                    new BaseStat(StatType.Life, StatDisplayType.Integer, 100, null),
                    new BaseStat(StatType.Mana, StatDisplayType.Integer, 100, null),
                },
                Level = 10,
                BasicAttack = new BaseAttack()
            };

            //subtract 10 life from the mob
            mob.Stats.Get(StatType.Life).CurrentValue -= 10;

            Console.WriteLine("\nMob Stats\n-----------------");
            foreach(var cStat in mob.Stats) {
                Console.WriteLine(cStat);
            }

            Console.ReadKey();
        }

    }
}
